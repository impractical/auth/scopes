module impractical.co/auth/scopes

replace impractical.co/auth/hmac v0.0.0 => ../hmac

require (
	bitbucket.org/ww/goautoneg v0.0.0-20120707110453-75cd24fc2f2c // indirect
	darlinggo.co/api v0.0.0-20160924005218-06eb95038fc2
	darlinggo.co/pan v0.1.0
	darlinggo.co/trout v1.0.1
	github.com/go-sql-driver/mysql v1.4.0 // indirect
	github.com/gobuffalo/packr v1.13.5 // indirect
	github.com/google/go-cmp v0.2.0
	github.com/google/pprof v0.0.0-20190502144155-8358a9778bd1 // indirect
	github.com/google/uuid v1.0.0 // indirect
	github.com/hashicorp/go-immutable-radix v1.0.0 // indirect
	github.com/hashicorp/go-memdb v0.0.0-20180223233045-1289e7fffe71
	github.com/hashicorp/go-uuid v1.0.0
	github.com/lib/pq v1.0.0
	github.com/mattn/go-sqlite3 v1.9.0 // indirect
	github.com/pborman/uuid v0.0.0-20180906182336-adf5a7427709 // indirect
	github.com/pkg/errors v0.8.0
	github.com/rubenv/sql-migrate v0.0.0-20180704111356-3f452fc0ebeb
	github.com/ziutek/mymysql v1.5.4 // indirect
	golang.org/x/arch v0.0.0-20190312162104-788fe5ffcd8c // indirect
	google.golang.org/appengine v1.1.0 // indirect
	gopkg.in/gorp.v1 v1.7.1 // indirect
	impractical.co/auth/hmac v0.0.0
	impractical.co/pqarrays v0.0.0-20170820231347-970404683d98
	yall.in v0.0.1
)
